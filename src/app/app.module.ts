import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { GameActionComponent } from './game-list/game-card/game-action/game-action.component';
import { CommonModule } from '@angular/common';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { GameListModule } from './game-list/game-list.module';
import { ImportsModule } from './imports.module';
import { GameListComponent } from './game-list/game-list.component';
import { GameCardComponent } from './game-list/game-card/game-card.component';
import { GameListFilterComponent } from './game-list/game-list-filter/game-list-filter.component';
import { GameFakeApiServiceService } from './services/game-fake-api-service.service';
import { GameCategoryFakeApiServiceService } from './services/game-category-fake-api-service.service';
import { VisitCounterService } from './services/visit-counter.service';
import { HttpClientModule } from '@angular/common/http';  
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatButtonModule, MatInputModule, MatSelectModule, MatPaginatorModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GameActionComponent,
    JwPaginationComponent,
    GameListComponent,
    GameListFilterComponent
  ],
  imports: [
    ImportsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    GameListModule
  ],
  providers: [
    GameCategoryFakeApiServiceService,
    GameFakeApiServiceService,
    VisitCounterService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
