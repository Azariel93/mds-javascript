import { Component, OnInit, Input } from '@angular/core';
import { Game } from 'src/app/models/game.model';
import { GameFakeApiServiceService } from 'src/app/services/game-fake-api-service.service';
import { GameApiService } from 'src/app/services/game-api.service';

@Component({
  selector: 'app-game-action',
  templateUrl: './game-action.component.html',
  styleUrls: ['./game-action.component.scss']
})
export class GameActionComponent implements OnInit {

  @Input()
  action: String;
  @Input()
  game: Game;

  constructor(private gameApi: GameApiService) { }

  ngOnInit() {}

  click(action: String) {
    let msg = '';
    switch (action) {
      case "Follow":
          msg = "I'm following " + this.game.title + " now";
        break;
      case "Share":
        msg = "Here we come, here we go, " + this.game.title + " Shared";
        break;
      case "Buy":
        msg = "I'll buy " + this.game.title + "!";
        break;
      case "Remove":
        msg = this.game.title + " removed !";
        break;
      default:
        msg = "Nothing's happening";
        break;
    }
    alert(msg);
  }
}
