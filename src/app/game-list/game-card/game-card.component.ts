import { Component, OnInit, Input } from '@angular/core';
import { Game } from 'src/app/models/game.model';
import { GameApiService } from 'src/app/services/game-api.service';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {

  @Input() game: Game;

  actionList: String[] = [
    'Follow',
    'Share',
    'Buy',
    'Remove'
  ];

  constructor(private gameApi: GameApiService) { }

  ngOnInit() {
  }

  limitDescription(text: string) {
    text = text.split("&#x200B;").join("");
    if (text.split(' ').length <= 20) {
      return text;
    } else {
      return text.split(' ', 20).join(' ') + '...';
    }
  }

}
