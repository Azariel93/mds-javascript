import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Genre } from '../../models/genre.model';
import { Publisher } from '../../models/publisher.model';

export interface GameFilter {
  title: string;
  genres: number[];
  publisher: string;
}

@Component({
  selector: 'app-game-list-filter',
  templateUrl: './game-list-filter.component.html',
  styleUrls: ['./game-list-filter.component.scss']
})
export class GameListFilterComponent implements OnInit {

  @Input() gameType: Genre[];
  @Input() publishers: Publisher[];

  /**
   * Formulaire pour récupérer les informations
   */
  // filtre = new FormGroup({
  //   title: new FormControl('',Validators.required),
  //   type: new FormControl(''),
  //   publisher: new FormControl('',Validators.required)
  // });

  /** The form. */
  readonly filtre = this.fb.group({
    title: '',
    genres: null,
    publisher: null
  });

  @Output()
  filter = new EventEmitter<GameFilter>();

  /** Functions called to emit a filter event to parent. */
  private readonly sendFilter = (filtre: GameFilter) => {
    // values.label = values.label.toLowerCase();
    this.filter.emit(filtre);
  }

  constructor(private readonly fb: FormBuilder) { }

  ngOnInit() {
    this.filtre
      .valueChanges
      .subscribe(this.sendFilter);
  }

  reset() {
    this.filtre.reset();
    this.filter.emit(this.filtre.value);
  }
}
