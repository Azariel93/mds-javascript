import { Component, OnInit } from '@angular/core';
import { Game } from '../models/game.model';
import { GameFilter } from './game-list-filter/game-list-filter.component';
import { GameFakeApiServiceService } from '../services/game-fake-api-service.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GameApiService } from '../services/game-api.service';
import { Genre } from '../models/genre.model';
import { GenreApiService } from '../services/genre-api.service';
import { PublisherApiService } from '../services/publisher-api.service';
import { Publisher } from '../models/publisher.model';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  size = 0;

  gameList = null;

  counter;

  games: Game[] = null;
  gamesFiltered: Game[] = null;
  genres$: Genre[] = null;
  publishers$: Publisher[] = null;

  /** Filters. */
  private filterValues: GameFilter;

  private readonly receiveGames = (games: Game[]) => {
    this.games = games;
    this.filterList();
  }

  constructor(private gameApi: GameApiService, 
              private genreApi: GenreApiService, private publisherApi: PublisherApiService) { }

  ngOnInit() {
    this.gameApi
        .getAll()
        .subscribe(this.receiveGames);
    
    this.counter = localStorage.getItem('counter');

    this.genreApi.getAll().subscribe(data => {
      this.genres$ = data;
    })
    
    this.publisherApi.getAll().subscribe(data => {
      this.publishers$ = data;
    })
  }

  plus() {
    switch (this.size) {
      case 100:
        this.size = 100;
        break;
      default:
        this.size += 10;
        break;
    }
  }

  minus() {
    switch (this.size) {
      case 0:
        this.size = 0;
        break;
      default:
        this.size -= 10;
        break;
    }
  }

  reset() {
    this.size = 0;
  }

  resize() {
    let style = {'width' : '23%'};
    style.width = `calc(23% + ${this.size}%)`;
    return style;
  }

  /** Called when user edit the filter form. */
  onFilter(values: GameFilter): void {
    this.filterValues = values;
    this.filterList();
  }

  onDelete(game: Game, event: MouseEvent) {
    event.stopPropagation();
    this.gameApi.delete(game)
      .subscribe(() => [this.games, this.games].forEach(l => l.splice(l.indexOf(game), 1)));
  }

  /** Uses {@link #tasks} & {@link #filterValues} to fill the {@link #filterTasks}. */
  private filterList(): void {    
    if (this.games) {
      if (this.filterValues) {        
        const title = this.filterValues.title;
        const genres  = this.filterValues.genres;
        const publisher  = this.filterValues.publisher; 

        this.gamesFiltered = this.games
            .filter(
              g => (!title || g.title.toLocaleLowerCase().includes(title.toLowerCase()))
                && (!genres || !genres.length || g.genres.some(genre => genres.includes(genre.id)))
                && (!publisher || g.publisher.id == + publisher));
      } else {
        this.gamesFiltered = this.games;
      }
    }
  }
}