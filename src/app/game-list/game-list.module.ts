import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameListComponent } from './game-list.component';
import { GameCardComponent } from './game-card/game-card.component';
import { GameListFilterComponent } from './game-list-filter/game-list-filter.component';
import { ImportsModule } from '../imports.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ImportsModule
  ]
})
export class GameListModule { }
