import { Genre } from './genre.model';
import { Developer } from './developper.model';
import { Publisher } from './publisher.model';

export class GameBase {
    id: number;
    title: string;
    description: string;
    // rating: number;
    coverImage: string;
}

export class Game extends GameBase{
    developer: Developer;
    publisher: Publisher;
    genres: Genre[];
}

export class GameDTO extends GameBase{
    developer: number;
    publisher: number;
    genres: number[];
}
