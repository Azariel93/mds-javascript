import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, combineLatest } from 'rxjs';

import { environment } from './../../environments/environment';
import { PublisherApiService } from './publisher-api.service';
import { DeveloperApiService } from './developer-api.service';
import { GenreApiService } from './genre-api.service';
import { tap, map, delay } from 'rxjs/operators';

import { Game, GameDTO } from '../models/game.model';
import { Genre } from '../models/genre.model';
import { Publisher } from '../models/publisher.model';
import { Developer } from '../models/developper.model';

const URL = `${environment.apiUrl}/games`;

@Injectable({
  providedIn: 'root'
})
export class GameApiService {

  private subject: ReplaySubject<Game[]> = new ReplaySubject(1);

  private neverCalled = true;

  constructor(private readonly http: HttpClient, private readonly developerApi: DeveloperApiService, 
              private readonly publisherApi: PublisherApiService, private readonly genreApi: GenreApiService) { }

  /** Returns all -cached- games. */
  getAll(): Observable<Game[]> {
    return combineLatest([
      this.http.get<GameDTO[]>(URL), 
      this.developerApi.getAll(), 
      this.publisherApi.getAll(), 
      this.genreApi.getAll()
    ])
      .pipe(
        map(([games, developers, publishers, genres]) => games.map(
          this.combineGameWithGenre(developers, publishers, genres)
        )),
        map(games => games as Game[]),
        tap(() => console.log("tap")),
        delay(1000)
      );
  }

  private combineGameWithGenre(developers, publishers, genres) {
    return (game: GameDTO) => ({
      ...game,
      developer: developers.find((d: Developer) => game.developer === d.id),
      publisher: publishers.find((p: Publisher) => game.publisher === p.id),
      genres: game.genres.map(genreId => genres.find((g: Genre) => genreId === g.id))
    });
  }
  
  /** Deletes the given todo. */
  delete(game: Game) {
    return this.http.delete<void>(`${URL}/${game.id}`);
  }
}
