import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, combineLatest } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Developer } from '../models/developper.model';

import { environment } from './../../environments/environment';

const URL = `${environment.apiUrl}/developers`;


@Injectable({
  providedIn: 'root'
})
export class DeveloperApiService {

  private subject: ReplaySubject<Developer[]> = new ReplaySubject(1);

  private neverCalled = true;

  /** Constructor. */
  constructor(private readonly http: HttpClient) { }

  /** Returns all -cached- genres. */
  getAll(): Observable<Developer[]> {
    if (this.neverCalled) {
      this.neverCalled = false;

      this.http
          .get<Developer[]>(URL)
          .subscribe(d => this.subject.next(d));
    }

    return this.subject;
  }
}
