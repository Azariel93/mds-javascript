import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GameCategoryFakeApiServiceService {

  baseUrl = `http://localhost:3000/category`; // base de l'url utilisable dans les requetes

  constructor(private http: HttpClient) { }

  getAll(): Observable<string[]>  {
    return this.http.get<string[]>(this.baseUrl);
  }
}
