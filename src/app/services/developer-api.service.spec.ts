import { TestBed } from '@angular/core/testing';

import { DeveloperApiService } from './developer-api.service';

describe('DeveloperApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeveloperApiService = TestBed.get(DeveloperApiService);
    expect(service).toBeTruthy();
  });
});
