import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import { Game } from '../models/game.model';
import { VisitCounterService } from './visit-counter.service';
import { games } from '../games';


@Injectable({
  providedIn: 'root'
})
export class GameFakeApiServiceService {

  baseUrl = `http://localhost:3000/games`; // base de l'url utilisable dans les requetes

  constructor(private http: HttpClient, private visitCounter: VisitCounterService) { }

  getAll(): Observable<Game[]> {
    // return this.http.get<Game[]>("./assets/games.json")
    // .pipe(
    //   delay(1000),
    //   tap(
    //     res => this.visitCounter.inc()
    //   )
    // );

    return of(games)
    .pipe(
      delay(1000),
      tap(
        res => this.visitCounter.inc()
      )
    );
  }

  removeGame(game: Game) {
    return this.getAll().subscribe(
      (items) => { 
        for(let item of items) {
          if (item == game) {
            items.splice(items.indexOf(item), 1);
          }
        } 
      }, (error) => {console.log(error)}, () => {} );

    // return of(games.splice(games.indexOf(game), 1))
    // .pipe(
    //   delay(1000),
    //   tap(
    //     res => this.visitCounter.inc()
    //   )
    // );
  }
}
