import { TestBed } from '@angular/core/testing';

import { PublisherApiService } from './publisher-api.service';

describe('PublisherApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublisherApiService = TestBed.get(PublisherApiService);
    expect(service).toBeTruthy();
  });
});
