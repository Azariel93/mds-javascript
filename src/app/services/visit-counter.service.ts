import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VisitCounterService {

  constructor() { }

  inc() {
    let counter: number;
    if (localStorage.getItem('counter') != null) {
      counter = + localStorage.getItem('counter');
    } else {
      counter = 0
    }
    counter += 1;
    localStorage.setItem('counter', counter.toString());
  }
}
