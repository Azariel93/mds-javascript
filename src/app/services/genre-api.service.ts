import { Injectable } from '@angular/core';
import { Genre } from '../models/genre.model';
import { Observable, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../environments/environment';

const URL = `${environment.apiUrl}/genres`;

@Injectable({
  providedIn: 'root'
})
export class GenreApiService {

  private subject: ReplaySubject<Genre[]> = new ReplaySubject(1);

  private neverCalled = true;

  /** Constructor. */
  constructor(private readonly http: HttpClient) { }

  /** Returns all -cached- genres. */
  getAll(): Observable<Genre[]> {
    if (this.neverCalled) {
      this.neverCalled = false;

      this.http
          .get<Genre[]>(URL)
          .subscribe(d => this.subject.next(d));
    }

    return this.subject;
  }
}
