import { TestBed } from '@angular/core/testing';

import { GenreApiService } from './genre-api.service';

describe('GenreApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenreApiService = TestBed.get(GenreApiService);
    expect(service).toBeTruthy();
  });
});
