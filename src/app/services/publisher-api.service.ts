import { Injectable } from '@angular/core';
import { Publisher } from '../models/publisher.model';
import { ReplaySubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../environments/environment';

const URL = `${environment.apiUrl}/publishers`;

@Injectable({
  providedIn: 'root'
})
export class PublisherApiService {

  private subject: ReplaySubject<Publisher[]> = new ReplaySubject(1);

  private neverCalled = true;

  /** Constructor. */
  constructor(private readonly http: HttpClient) { }

  /** Returns all -cached- genres. */
  getAll(): Observable<Publisher[]> {
    if (this.neverCalled) {
      this.neverCalled = false;

      this.http
          .get<Publisher[]>(URL)
          .subscribe(d => this.subject.next(d));
    }

    return this.subject;
  }
}
